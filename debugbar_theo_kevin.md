# MVC Debug Bar

## Installation
- Mettre le dossier debug dans */asset*


- Mettre le fichier debug.js dans */asset*


- Copier/coller les routes ci-dessous dans *config/routes.php*
_________________________________________________
```php
/* config/routes.php */

    array('get-routes','DebugBar','getroutes'),
    array('get-error','DebugBar','geterror'),
    array('get-global','DebugBar','getglobal'),
    array('get-request','DebugBar','getrequest'),
    array('get-personalisation','DebugBar','ergonomie'),
    array('set-personalisation','DebugBar','setergonomie'),
    array('login','DebugBar','login'),
    array('logout','DebugBar','logout'),
```
_________________________________________________


- Mettre le fichier DebugBarController.php dans *src/Controller*


- Dans chaque fichier php de layout ajouter le add\_webpack\_style et le add\_webpack\_script, ainsi que les cinq lignes contenant la div debug-bar.

_________________________________________________
```php
/* template/layout/ */
   <?php echo $view->add_webpack_style('debug'); echo $view->add_webpack_style('app');?>
   
   ...

    <?php
    $config = include  dirname(__DIR__) . '../../config/config-dist.php';
    if ($config['mode'] == 'dev'){
        echo '<div id="debug-bar" class="debug-bar"></div>';
    }

    echo $view->add_webpack_script('debug'); echo $view->add_webpack_script('app'); ?>
```
_________________________________________________


- Copier la ligne **debug: './asset/debug.js',** dans le fichier *webpack.config.js*

_________________________________________________
```js
/* webpack.config.js */

    module.exports = {
        mode: "development", // production || development
        devtool: 'inline-source-map',
        entry: {
            debug: './asset/debug.js',
            app: './asset/app.js',
        },
```
_________________________________________________


- Copier le fichier DebugError.php dans *src/Service*


- Appeler le gestionnaire d'erreur dans le fichier *public/index.php*

_________________________________________________
```php
/* public/index.php */
   
    // Instance de PDO
    $app = \Core\App::getInstance();


    //Debug error
    $error=new App\Service\DebugError();
    $error->getError();
    
    
    // Routes
    require('../config/routes.php');
    $router = new \Core\Kernel\Router($routes);

```
_________________________________________________


- Copier la private fonction saveSqlRequest(), et l'appeler au debut des fonctions **query()**, **prepare()**, et **prepareInsert()** dans le fichier *core/Kernel/Database.php*

_________________________________________________
```php
/* core/Kernel/Database.php */

    public function query($sql, $className)
    {
        $this->saveSqlRequest($sql);
        $query = $this->getPdo()->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_CLASS,$className);
    }
    
    
    ...
    
    
    public function prepare($sql, $bind, $className, $one = false)
    {
        $this->saveSqlRequest($sql);
        $query = $this->getPdo()->prepare($sql);
        $query->execute($bind);
        $query->setFetchMode(PDO::FETCH_CLASS,$className);
        if($one) {
            return $query->fetch();
        } else {
            return $query->fetchAll();
        }
    }
    
    
    ...
    
    
    public function prepareInsert($sql, $bind)
    {
        $this->saveSqlRequest($sql);
        $query = $this->getPdo()->prepare($sql);
        $query->execute($bind);
    }
    
    
    ...
    
    
    private function saveSqlRequest($sql){
        $config = include  dirname(__DIR__) . '../../config/config-dist.php';
        if ($config['mode'] == 'dev'){
            $_SESSION['debug_bar_sql'][]= $sql;
        }
    }

```
_________________________________________________


- Ajouter **'mode' => 'dev'** dans le fichier *config/config-dist.php*

_________________________________________________
```php
/* config/config-dist.php */

    <?php

    return array(
        'db_name'   => 'mvcdebugbar',
        'db_user'   => 'root',
        'db_pass'   => '',
        'db_host'   => 'localhost',
    
        'version' => '1.0.0',
    
        'mode' => 'dev'   //dev ou prod
    );

```
_________________________________________________

## Utilisation

Pour dérouler la Debug Barre il suffit d'appuyer sur le bouton engrenage et bas de l'écran, vous aurez alors de nombreuses options disponibles : 

- Info Page qui vous permettra d'obtenir les informations de la page actuelle


- Performance qui vous permettra d'observer les performances de votre site


- Error qui vous permettra de voir toutes les erreurs Php et Js de votre site


- Log qui vous permettra de tester votre site avec plusieurs rôles


- Routes qui vous donnera toutes les routes de votre site


- Requêtes qui vous donnera toutes les requêtes Sql de votre site


- $Global qui vous donnera le contenu de toutes les principals SuperGlobal du site, excepté $_POST


- Personalisation qui vous permettra de choisir les coté de votre debug barre ainsi que sa couleur, vous pouvez personnaliser vos thèmes de couleurs dans *\debug\settingDebug.js*


- Vous pouvez personnalisé les couleurs de votre debug barre ainsi que les roles pour le log dans *asset/debug/settingDebug.js*



## Auteurs

- [@kevinquene](https://www.gitlab.com/kevinquene)
- [@Theo_Leroux](https://www.gitlab.com/Theo_Leroux)