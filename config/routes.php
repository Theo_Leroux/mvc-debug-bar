<?php

$routes = array(
    array('home','default','index'),

    array('get-routes','DebugBar','getroutes'),
    array('get-error','DebugBar','geterror'),
    array('get-global','DebugBar','getglobal'),
    array('get-request','DebugBar','getrequest'),
    array('get-personalisation','DebugBar','ergonomie'),
    array('set-personalisation','DebugBar','setergonomie'),
    array('login','DebugBar','login'),
    array('logout','DebugBar','logout'),
);


