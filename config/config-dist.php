<?php

return array(
    'db_name'   => 'mvcdebugbar',
    'db_user'   => 'root',
    'db_pass'   => '',
    'db_host'   => 'localhost',

    'version' => '1.0.0',

    'mode' => 'dev'   //dev ou prod
);
