import {host} from "./settingDebug";

export async function getGlobal() {
    let params = new FormData();
    const res = await fetch(host()+'/get-global', {
        body: params,
        method: 'post',
    });
    const data = await res.json();

    const globalBox = document.querySelector('#debug-bar-dropdown-global-content')

    let paramsget = new URLSearchParams(window.location.search);
    data['get'] = [];
    for (const [nom, valeur] of paramsget) {
        data['get'].push({'key' : nom,'value' : valeur});
    }

    if (data['get'].length == 0 && data['post'].length == 0 && data['session'].length == 0 && data['cookie'].length == 0 && data['file'].length == 0){
        let ul = document.createElement('ul')
        ul.id="listeGet"
        ul.classList.add('liste-element')

        ul.innerText = 'Toutes les Globals du site sont vide'
        ul.style.textAlign = 'center';
        globalBox.append(ul)
    }

    if (data['get'].length > 0){
        let ul = document.createElement('ul')
        ul.id="listeGet"
        ul.classList.add('liste-element')

        ul.innerHTML = ul.innerHTML+'$_Get : '

        for (let i =0; i<data['get'].length; i++){
            let li = document.createElement('li');
            li.classList.add('element');
            li.innerText = data['get'][i]['key']+' = '+JSON.stringify(JSON.parse(JSON.stringify(data['get'][i]['value'])), null, 4);
            ul.appendChild(li)
        }
        if (globalBox){
            globalBox.append(ul)
        }
    }

    if (data['post'].length > 0){
        let ul = document.createElement('ul')
        ul.id="listePost"
        ul.classList.add('liste-element')

        ul.innerHTML = ul.innerHTML+'$_Post : '

        for (let i =0; i<data['post'].length; i++){
            let li = document.createElement('li');
            li.classList.add('element');
            li.innerText = data['post'][i]['key']+' = '+JSON.stringify(JSON.parse(JSON.stringify(data['post'][i]['value'])), null, 4);
            ul.appendChild(li)
        }
        if (globalBox){
            globalBox.append(ul)
        }
    }

    if (data['session'].length > 0){
        let ul = document.createElement('ul')
        ul.id="listeSession"
        ul.classList.add('liste-element')

        ul.innerHTML = ul.innerHTML+'$_Session : '

        for (let i =0; i<data['session'].length; i++){
            let li = document.createElement('li');
            li.classList.add('element');
            li.innerText = data['session'][i]['key']+' = '+JSON.stringify(JSON.parse(JSON.stringify(data['session'][i]['value'])), null, 4);
            ul.appendChild(li)
        }
        if (globalBox){
            globalBox.append(ul)
        }
    }

    if (data['cookie'].length > 0){
        let ul = document.createElement('ul')
        ul.id="listeCookie"
        ul.classList.add('liste-element')

        ul.innerHTML = ul.innerHTML+'$_Cookie : '

        for (let i =0; i<data['cookie'].length; i++){
            let li = document.createElement('li');
            li.classList.add('element');
            li.innerText = data['cookie'][i]['key']+' = '+JSON.stringify(JSON.parse(JSON.stringify(data['cookie'][i]['value'])), null, 4);
            ul.appendChild(li)
        }
        if (globalBox){
            globalBox.append(ul)
        }
    }

    if (data['file'].length > 0){
        let ul = document.createElement('ul')
        ul.id="listeFile"
        ul.classList.add('liste-element')

        ul.innerHTML = ul.innerHTML+'$_File : '

        for (let i =0; i<data['file'].length; i++){
            let li = document.createElement('li');
            li.classList.add('element');
            li.innerText = data['file'][i]['key']+' = '+JSON.stringify(JSON.parse(JSON.stringify(data['file'][i]['value'])), null, 4);
            ul.appendChild(li)
        }
        if (globalBox){
            globalBox.append(ul)
        }
    }
}