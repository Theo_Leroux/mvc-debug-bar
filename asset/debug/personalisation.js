import {host} from "./settingDebug";
import {dropdownInfo} from "./settingDebug";
import {dropdownStyle} from "./settingDebug";

const debugBar = document.querySelector('#debug-bar')
const dropdowns = dropdownInfo();
const openDebug = document.querySelector('#debug-bar-open-button')
const closeDebug = document.querySelector('.debug-bar-close-button')

function setPosition(position){
    let openDebugVerif = document.querySelector('#debug-bar-open-button')
    let closeDebugVerif = document.querySelector('.debug-bar-close-button')

    if (openDebugVerif){
        if (position == 'left'){
            debugBar.style.right = 'initial';
            debugBar.style.left = '0px';
            debugBar.style.top = '0px';
            debugBar.style.bottom = 'initial';
            debugBar.style.width = '450px';
            debugBar.style.height = '100vh';

            openDebugVerif.style.left = '30px'
            openDebugVerif.style.right = 'initial'
            closeDebugVerif.style.left = '15px'
            closeDebugVerif.style.right = 'initial'
        }
        if (position == 'right'){
            debugBar.style.right = '0px';
            debugBar.style.left = 'initial';
            debugBar.style.top = '0px';
            debugBar.style.bottom = 'initial';
            debugBar.style.width = '450px';
            debugBar.style.height = '100vh';

            openDebugVerif.style.left = 'initial'
            openDebugVerif.style.right = '30px'
            closeDebugVerif.style.left = 'initial'
            closeDebugVerif.style.right = '15px'
        }
    }
}

function setColor(color){
    let openDebugVerif = document.querySelector('#debug-bar-open-button')
    let closeDebugVerif = document.querySelector('.debug-bar-close-button')
    if (openDebugVerif){
        let themes = dropdownStyle();
        for (let i=0; i<themes.length; i++){
            if (color == themes[i]['number']){
                debugBar.style.backgroundColor = themes[i]['debugBarColor']
                openDebugVerif.style.backgroundColor = themes[i]['debugBarColor']
                debugBar.style.color = themes[i]['debugBarTextColor']
                openDebugVerif.querySelector('i').style.color = themes[i]['debugBarOpenButtonColor']
                closeDebugVerif.querySelector('i').style.color = themes[i]['debugBarCloseButtonColor']
                let borderColor=' 0 1px 0 0 '+themes[i]['debugBarBorderColor'];
                let backContentColor = themes[i]['debugBarContentColor'];
                for (let i = 0; i<dropdowns.length; i++){
                    let openDropdown = document.querySelector(dropdowns[i]['open']).style.boxShadow = borderColor;
                    let dropdownContent = document.querySelector(dropdowns[i]['content']).style.boxShadow = borderColor;
                    dropdownContent = document.querySelector(dropdowns[i]['content']).style.backgroundColor = backContentColor;
                }
            }
        }
    }
}

async function updateDebugBar(type, value) {

    let params = new FormData();
    params.append('type', type)
    params.append('value', value)
    const res = await fetch(host() + '/set-personalisation', {
        body: params,
        method: 'post',
    });
}

export async function getPersonalisation() {

    let params = new FormData();
    const res = await fetch(host()+'/get-personalisation', {
        body: params,
        method: 'post',
    });
    const data = await res.json();

    setPosition(data['position'])
    setColor(data['theme'])

    const persoBox = document.querySelector('#debug-bar-dropdown-personalisation-content')
    let ul = document.createElement('ul')

    ul.id="listePersonalisation"
    ul.classList.add('liste-element')

    ul.innerHTML = 'Position de la Debug Bar : '

    let selectPosition = document.createElement('select');
    let optionLeft = document.createElement('option');
    let optionRight = document.createElement('option');

    optionLeft.innerText = 'À Gauche';
    optionRight.innerText = 'À Droite';
    optionLeft.value = 'left';
    optionRight.value = 'right';

    selectPosition.addEventListener('change', function() {
        setPosition(selectPosition.value)
        updateDebugBar('position', selectPosition.value)
    });

    selectPosition.appendChild(optionRight)
    selectPosition.appendChild(optionLeft)
    ul.appendChild(selectPosition)

    if (data['position'] == 'left'){
        optionLeft.selected=true;
    }
    if (data['position'] == 'right'){
        optionRight.selected=true;
    }
    if (persoBox){
        persoBox.append(ul)
    }


    ul = document.createElement('ul')

    ul.id="listePersonalisation"
    ul.classList.add('liste-element')

    ul.innerHTML = 'Couleur de la Debug Bar : '

    let selectCouleur = document.createElement('select');
    let optionCouleur1 = document.createElement('option');
    let optionCouleur2 = document.createElement('option');
    let optionCouleur3 = document.createElement('option');
    let optionCouleur4 = document.createElement('option');
    let optionCouleur5 = document.createElement('option');

    optionCouleur1.innerText = 'Theme 1';
    optionCouleur2.innerText = 'Theme 2';
    optionCouleur3.innerText = 'Theme 3';
    optionCouleur4.innerText = 'Theme 4';
    optionCouleur5.innerText = 'Theme 5';
    optionCouleur1.value = 1;
    optionCouleur2.value = 2;
    optionCouleur3.value = 3;
    optionCouleur4.value = 4;
    optionCouleur5.value = 5;

    selectCouleur.addEventListener('change', function() {
        setColor(selectCouleur.value)
        updateDebugBar('theme', selectCouleur.value)
    });

    selectCouleur.appendChild(optionCouleur1)
    selectCouleur.appendChild(optionCouleur2)
    selectCouleur.appendChild(optionCouleur3)
    selectCouleur.appendChild(optionCouleur4)
    selectCouleur.appendChild(optionCouleur5)
    ul.appendChild(selectCouleur)

    if (data['theme'] == 1){
        optionCouleur1.selected=true;
    }
    if (data['theme'] == 2){
        optionCouleur2.selected=true;
    }
    if (data['theme'] == 3){
        optionCouleur3.selected=true;
    }
    if (data['theme'] == 4){
        optionCouleur4.selected=true;
    }
    if (data['theme'] == 5){
        optionCouleur5.selected=true;
    }

    if (persoBox){
        persoBox.append(ul)
    }
}