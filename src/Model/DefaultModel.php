<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class DefaultModel extends AbstractModel
{

    /**
     * @return string
     */
    public static function getTable(): string
    {
        return self::$table;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    public static function insertUser()
    {
        return App::getDatabase()->prepareInsert("INSERT INTO `user` (`email`, `role`, `password`,) VALUES ( 'test@gmail.com', 'admin', '123456789azerty' )", []);
    }

    protected static $table = 'user';

    protected $id;
    protected $email;
    protected $role;
    protected $password;

}
