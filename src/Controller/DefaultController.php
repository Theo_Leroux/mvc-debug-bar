<?php

namespace App\Controller;


use App\Model\DefaultModel;
use Core\Kernel\AbstractController;


/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {
        $message = 'Bienvenue sur le framework MVC';
        $info=$_SERVER['HTTP_USER_AGENT'];

        $this->render('app.default.frontpage',array(
            'message' => $message,
            'info'=>$info,
        ));

    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
